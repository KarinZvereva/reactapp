const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.join(__dirname, "/bundle"),
    filename: "index_bundle.js"
  },
  devServer: {
     inline: true,
     port: 8001
  },
  module: {
    rules: [
       {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
       },
       {
        test: /\.(png|jpg|svg)$/,
        loader: 'url-loader'
       },
       {
         test: /\.(scss|css)$/,
         exclude: /node_modules/,
         use: [ 
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader",
              options: {
                modules: {
                  // modules: true,
                  localIdentName: '[local]'
                }
              }
            },
            {
              loader: "sass-loader"
            }
          ]
       }
    ]
 },
 plugins: [
   new HtmlWebpackPlugin({
     template: "./src/index.html"
   }),
   new MiniCssExtractPlugin({
    filename: 'style.css',
   })
 ]
};
