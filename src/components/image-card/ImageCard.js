import React, { Component } from 'react';
import MoreVertTwoToneIcon from '@material-ui/icons/MoreVertTwoTone';
import { connect } from 'react-redux';
import './imagecard.scss';

class ImageCard extends Component{

    constructor(props) {
        super(props);
        this.state = {
            visible: true
        }
        this.removeCard = this.removeCard.bind(this);
        this.zoomIn = this.zoomIn.bind(this);
    }

    removeCard(e){
        e.stopPropagation();
        let url = 'http://localhost:5000/data/';
        fetch(url + this.props.data.id,{
            method: 'DELETE'
        })
        .then(response => {
            if(response.status == 200)
                this.props.onRemoveCard(this.props.data.id);
        })
    }

    zoomIn(){
        this.props.onZoom(this.props.data);
    }

    render(){
        const data = this.props.data;
        if(this.state.visible){
            return(
                <div className='container__dashboard__image-card' key={data.desc} onClick={this.zoomIn}>
                        {/* <MoreVertTwoToneIcon className="dots" aria-label="moredetails" titleAccess="more"/> */}
                        <div className='container__dashboard__image-card__top-block'>
                            <a href="#" onClick={this.removeCard}>Remove</a>
                            <a href="#" onClick={(e) => {e.stopPropagation(), this.props.showFormCard('EDIT', this.props.data)}}>Edit</a>
                        </div>
                        <img src={data.src} />
                        <div className='container__dashboard__image-card__bottom-block'>
                            <div className='container__dashboard__image-card__short-desc'>
                                <div>{data.name}</div>
                                <div>{data.year}</div>
                            </div>
                        </div> 
                </div>
            );
        }
        return null;
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        onRemoveCard: (id) => {
            dispatch({ type: 'REMOVE_CARD', card: {
                    "id": id
                }   
            });
        }
    })
)(ImageCard);
