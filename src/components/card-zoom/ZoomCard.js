import React, { Component } from 'react';
import './cardzoom.scss';

class CardZoom extends Component{
    constructor(props) {
        super(props);
        this.state = {
            'src': props.cardzoom.src,
            'name': props.cardzoom.name,
            'year': props.cardzoom.year,
            'desc': props.cardzoom.desc
        }
    }

    stop(e){
        e.stopPropagation();
    }

    render(){
        return (
            <div className="form-zoom-card" onClick={() => this.props.removeFormCard()}>
                <div className="form-zoom-card__background" onClick={this.stop}>
                    <div className="form-zoom-card__background__top-block">
                        <img src={this.state.src}/>
                        <div className="form-zoom-card__background__top-block__info">
                            <div>{this.state.name}</div>
                            <div>{this.state.year}</div>
                        </div>
                    </div>
                    <div className="form-zoom-card__background__desc-block">{this.state.desc}</div>
                </div>
            </div>
        );
    }
}

export default CardZoom;