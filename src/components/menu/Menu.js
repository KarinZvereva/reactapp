import React, { Component } from 'react';
import './menu.scss';

class Menu extends Component{
   render(){
      return(
        <div className="container__menu">
            <div className="container__menu__title">
              Art gallery
            </div>
            <div className="btn" onClick={() => this.props.showFormCard('ADD', {
                'name': '',
                'desc': '',
                'year': ''
            })}>
              <i className="icon icon-add"></i>
              <span>Add</span>
            </div>
        </div>
      );
   }
}
export default Menu;