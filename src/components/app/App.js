import React, { Component } from 'react';
import Menu from '../menu/Menu.js';
import Card from '../image-card/ImageCard.js';
import FormCard from '../add-card/AddCard.js';
import CardZoom from '../card-zoom/ZoomCard.js';
import { connect } from 'react-redux';
import './app.scss';

class App extends Component{
   
   constructor(props) {
      super(props);
      this._isMounted = false;
      this.state = {
         form: '',
         countCards: 0
      };
      this.showFormCard = this.showFormCard.bind(this);
      this.removeFormCard = this.removeFormCard.bind(this);
      this.zoomCard = this.zoomCard.bind(this);
   }

   
   removeFormCard(){
      this.setState({form: null });
   }

   showFormCard(type, _card){
      this.setState({form: <FormCard type={type} removeFormCard={this.removeFormCard} editcard={_card} /> });
   }

   zoomCard(cardzoom){
      this.setState({form: <CardZoom cardzoom={cardzoom} removeFormCard={this.removeFormCard} /> })
   }

   componentDidMount(){
      this._isMounted = true;
      if(this._isMounted)
         this.props.onFetchCards();
   }

   componentWillUnmount(){
      this._isMounted = false;
   }

   render(){
      return(
         <div className='container'>
            <Menu showFormCard={this.showFormCard}/>
            <div className="container__dashboard">
               {
                  this.props.cards.map( elem => 
                     <Card data={elem} key={elem.id} showFormCard={this.showFormCard} onZoom={this.zoomCard}/>
                  )
               }
            </div>
            {this.state.form}
         </div>
      );
   }
}
export default connect(
   state => ({
      cards: state.data
   }),
   dispatch => ({
      onFetchCards: () => {
         let url = 'http://localhost:5000/data';
         fetch(url)
         .then(respone => respone.json())
         .then(data => {
            data.forEach( elem => {
               dispatch({
                  type: 'ADD_CARD',
                  card: elem
               })
            })
         })
      }
   })
)(App);