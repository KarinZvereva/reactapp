import React, { Component } from 'react';
import { connect } from 'react-redux';
import './addcard.scss';
class AddCardButton extends Component{
    render(){
        return(
            <div className="btn" onClick={() => this.props.showFormCard()}>
                <i className="icon icon-add"></i>
                <span>Add</span>
            </div>
        );
    }
}

class FormCard extends Component{
    constructor(props){
        super(props);
        this.insertOrEdit = this.insertOrEdit.bind(this);

        this.state = {
            name: this.props.editcard.name,
            year: this.props.editcard.year,
            desc: this.props.editcard.desc
        }
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeYear = this.handleChangeYear.bind(this);
        this.handleChangeDesc = this.handleChangeDesc.bind(this);
    }

    stop(e){
        e.stopPropagation();
    }

    handleChangeName(e){
        this.setState({
            name: e.target.value
        })
    }

    handleChangeYear(e){
        this.setState({
            year: e.target.value
        })
    }

    handleChangeDesc(e){
        this.setState({
            desc: e.target.value
        })
    }

    insertOrEdit(){

        const name = this.state.name;
        const year = this.state.year;
        const desc = this.state.desc;

        if(this.props.type === 'ADD'){

            let card = {
                id: this.props.maxId + 1,
                src: "../src/resourses/images/img-whiteflowers.jpg",
                name: name,
                year: year,
                desc: desc
            }
            let url = 'http://localhost:5000/data';
            fetch(url,{
                    method: 'POST',
                    body: JSON.stringify(card),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(response => {
                    if(response.status === 201)
                        this.props.onInsertCard(card);
                    this.props.removeFormCard();
                })
                .catch(err => console.log(err))

        } else{
            if(this.props.type === 'EDIT'){
                let card = {
                    id: this.props.editcard.id,
                    src: this.props.editcard.src,
                    name: name,
                    year: year,
                    desc: desc
                }

                let url = 'http://localhost:5000/data/';
                fetch(url + card.id,{
                        method: 'PUT',
                        body: JSON.stringify(card),
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(response => {
                        if(response.status === 200)
                            this.props.onEditCard(card);
                        this.props.removeFormCard();
                    })
                    .catch(err => console.log(err))
            }
        }

        
    }

    render(){
        return(
            <div className="form-add-card" onClick={() => this.props.removeFormCard() }>
                <div className="form-add-card__background" onClick={this.stop}>
                <div>{this.props.type}</div>
                    <div className="form-add-card__background__content">
                        <input type="text" placeholder="Name" onChange={this.handleChangeName} value={this.state.name}></input>
                        <input type="number" placeholder="Year" onChange={this.handleChangeYear} value={this.state.year}></input>
                        <textarea placeholder="Description" onChange={this.handleChangeDesc} value={this.state.desc}></textarea>
                    </div>
                    <button onClick={this.insertOrEdit}>Add</button>
                </div>
            </div>
        );
    }
}


export default connect(
    state => ({
        maxId: state.maxId
    }), 
    dispatch => ({
        onInsertCard: (card) => {
            dispatch({
                type: 'ADD_CARD',
                card: card
            })
        },
        onEditCard: (card) => {
            dispatch({
                type: 'EDIT_CARD',
                card: card
            })
        }
    })
)(FormCard);

