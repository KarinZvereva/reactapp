import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider, connect } from 'react-redux'
import App from './components/app/App.js';


// const list = {
//         'maxId': 10,
//         'data': [{
//             "id": 10,
//             "src": "../src/resourses/images/img-lake.jpg",
//             "name": "TEST",
//             "year": "0000"
//         }]
//     }

const list = {
    'maxId': 0,
    'data': []
}

function cards(state = list, action){

    // console.log('Action')
    // console.log(action)
    //sd

    switch(action.type){
        case 'ADD_CARD': 
            let maxId = (action.card.id > state.maxId) ? action.card.id : state.maxId;
            return {
                'maxId': maxId,
                'data': [
                    ...state.data,
                    action.card
                ]
            }
        case 'REMOVE_CARD':
            return {
                'maxId': state.maxId,
                'data': state.data.filter( item => item.id !== action.card.id )
            }
        case 'EDIT_CARD':
            return {
                'maxId': state.maxId,
                'data': state.data.map( item => {
                    if(item.id == action.card.id){
                        return action.card
                    }
                    return item;
                })
            }
    }

    return state;
}
const store = createStore(cards);



store.subscribe(() => {
    console.log("Subscribe")
    console.log(store.getState());
});


ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);